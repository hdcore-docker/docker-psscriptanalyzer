#!/bin/sh

# Author: Danny Herpol <hdcore@lachjekrom.com>
# Version GIT: 2023-10-21 08:37

# Invoke-ScriptAnalyzer.sh
# Start the PSScriptAnalyzer via powershell

if [ "$#" -eq 0 ]
then
    echo "Not enough parameters for Invoke-ScriptAnalyzer.sh"
    exit 1
fi
if [ "$1" = "--version" ]
then
    pwsh -Command "& {Import-Module PSScriptAnalyzer && Get-Module PSScriptAnalyzer | Select-Object -Property Name,Version}"
    exit 0
fi

# shellcheck disable=SC2145
pwsh -Command "& {Import-Module PSScriptAnalyzer && Invoke-ScriptAnalyzer -Recurse -EnableExit $@ | Format-List -Property Severity,Rulename,ScriptPath,Line,Column,Message}"
