#!/bin/sh

# Author: Danny Herpol <hdcore@lachjekrom.com>
# Version GIT: 2023-03-29 20:23

# docker-entrypoint.sh 
# for hdcore docker-psscriptanalyzer image

CGREEN="\e[32m"
CNORMAL="\e[0m"

# shellcheck disable=SC2059
printf "== ${CGREEN}Start entrypoint ${0}${CNORMAL} ==\n"

printf "HDCore docker-psscriptanalyzer container image\n"
printf "Installed version of PSScriptAnalyzer:\n"
pwsh --version
pwsh -Command "& {Import-Module PSScriptAnalyzer && Get-Module PSScriptAnalyzer | Select-Object -Property Name,Version}"
#flake8 --version

# shellcheck disable=SC2059
printf "== ${CGREEN}End entrypoint ${0}${CNORMAL} ==\n"

# Execute docker CMD
exec "$@"