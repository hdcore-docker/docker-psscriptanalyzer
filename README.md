# HDCore - docker-psscriptanalyzer

## Introduction

This is a small container image that contains a powershell7 environment with psscriptanalyzer. This container is very usefull for automatic testing during CI.

## Image usage

- Run psscriptanalyzer:

```bash
docker run --rm -v /path/to/python/code:/code hdcore/docker-psscriptanalyzer:<version> Invoke-ScriptAnalyzer.sh <arguments>
```

- Run shell:

```bash
docker run -it --rm -v /path/to/python/code:/code hdcore/docker-psscriptanalyzer:<version> /bin/sh
```

- Run powershell:

```bash
docker run -it --rm -v /path/to/python/code:/code hdcore/docker-psscriptanalyzer:<version> pwsh
```

- Use in .gitlab-ci.yml:

```bash
image: hdcore/docker-psscriptanalyzer:<version>
script: Invoke-ScriptAnalyzer.sh <arguments>
```

## Available tags

- hdcore/docker-psscriptanalyzer:1

## Container Registries

The image is stored on multiple container registries at dockerhub and gitlab:

- Docker Hub:
  - hdcore/docker-psscriptanalyzer
  - registry.hub.docker.com/hdcore/docker-psscriptanalyzer
- Gitlab:
  - registry.gitlab.com/hdcore-docker/docker-psscriptanalyzer

## Building image

Build:

```bash
docker build -f <version>/Dockerfile -t docker-psscriptanalyzer:<version> .
```
